using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class LobbyConnectionManager : MonoBehaviourPunCallbacks
{
    [SerializeField] TMP_InputField PlayerNameInput;
    [SerializeField] TMP_InputField RoomNameInputField;

    [SerializeField] Text _Statustext;


    [SerializeField] GameObject CreateRoomPanel;


    [SerializeField] Button LoginBtn;
    [SerializeField] Button CreateRoonBtn;
    [SerializeField] Button EnterRoomBtn;

    [SerializeField] GameObject CanvasObj;



    // Start is called before the first frame update
    void Start()
    {
        LoginBtn.onClick.AddListener(OnLoginButtonClicked);
        CreateRoonBtn.onClick.AddListener(OnCreateRoomButtonClicked);
        EnterRoomBtn.onClick.AddListener(OnJoinRoomClicked);
    }

    // Update is called once per frame
    void Update()
    {
        _Statustext.text = "Status : "+PhotonNetwork.NetworkClientState;
    }

    public void OnLoginButtonClicked()
    {
        string playerName = PlayerNameInput.text;

        if (!playerName.Equals(""))
        {
            PhotonNetwork.LocalPlayer.NickName = playerName;
            PhotonNetwork.ConnectUsingSettings();
        }
        else
        {
            Debug.LogError("Player Name is invalid.");
        }
    }


    public void OnCreateRoomButtonClicked()
    {
        string roomName = RoomNameInputField.text;
        roomName = (roomName.Equals(string.Empty)) ? "Room " + Random.Range(1000, 10000) : roomName;

        RoomOptions options = new RoomOptions { MaxPlayers = 2, PlayerTtl = 10000 };

        PhotonNetwork.CreateRoom(roomName, options, null);
    }

    public void OnJoinRoomClicked()
    {
        PhotonNetwork.JoinRoom(RoomNameInputField.text);

    }

    #region PUN CALLBACKS

    public override void OnConnectedToMaster()
    {
        //this.SetActivePanel(SelectionPanel.name);
    }

    public override void OnConnected()
    {
        Debug.Log("Player Connected : " + PhotonNetwork.LocalPlayer.NickName);
        CreateRoomPanel.SetActive(true);
    }


    public override void OnCreatedRoom()
    {
        Debug.Log("Room Created");
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);
    }

    public override void OnJoinedRoom()
    {
        CanvasObj.SetActive(false);
    }

    #endregion
}
