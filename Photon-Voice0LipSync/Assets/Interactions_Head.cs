using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using Photon.Voice.PUN;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Interactions_Head : MonoBehaviour
{
    [SerializeField]
    private Image recorderSprite;

    [SerializeField]
    private Image speakerSprite;

    [SerializeField] PhotonVoiceView photonVoiceView;
    // Start is called before the first frame update
    [SerializeField] PhotonView pv;
    [SerializeField] Animator HeadAniator;
    void Start()
    {
        HeadAniator= this.GetComponent<Animator>();
        pv = this.GetComponent<PhotonView>();
    }

    // Update is called once per frame
    void Update()
    {
        if (pv.IsMine)
        {
            //this.recorderSprite.enabled = this.photonVoiceView.IsRecording;
            //this.speakerSprite.enabled = this.photonVoiceView.IsSpeaking;
            HeadAniator.SetBool("OnTalking", this.photonVoiceView.IsRecording);
        }
           // this.bufferLagText.enabled = this.showSpeakerLag && this.photonVoiceView.IsSpeaking;
            //if (this.bufferLagText.enabled)
            //{
            //    this.bufferLagText.text = string.Format("{0}", this.photonVoiceView.SpeakerInUse.Lag);
            //}
        
    }
}
